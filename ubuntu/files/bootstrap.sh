#!/bin/bash
# https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10

# Update your existing list of packages
apt update

# Install a few prerequisite packages which let "apt" use packages over https
apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common

# Add GPG key from the official docker repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add docker apt repo
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update the package database with the Docker packages from the newly added repo
apt update

# Make sure you are about to install from the docker repo instance of the default ubuntu repo.
apt-cache policy docker-ce

# Install docker
apt install -y docker-ce

# Status docker
systemctl status docker