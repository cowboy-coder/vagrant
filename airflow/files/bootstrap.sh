#!/bin/bash

# Bootstrap
export AF_USER="airflow"
useradd -c "Airflow user." -m -s /bin/bash "${AF_USER}"
mkdir -p /var/log/airflow
chown -R ${AF_USER}:${AF_USER} /var/log/airflow

# Conda
/bin/su -c "/vagrant/files/install_conda.sh" - "${AF_USER}"

# Airflow
/bin/su -c "/vagrant/files/install_airflow.sh" - "${AF_USER}"
