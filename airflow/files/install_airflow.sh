#!/bin/bash

export PATH="/home/airflow/miniconda3/bin:$PATH"

# Create conda environment
conda create -q -n sandbox python=3.9 -y
conda init bash
source activate sandbox
echo "source activate sandbox" >> ~/.bashrc

# Install airflow
pip install apache-airflow==2.1.0

# Initialize airflow
airflow users create -u admin -p admin -f Sean -l Humbarger -r Admin -e admin@airflow.com
airflow db init
airflow webserver >> /var/log/airflow/webserver.log &
airflow scheduler >> /var/log/airflow/scheduler.log &
