#!/bin/bash

# Install Miniconda
wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh -b -p /home/airflow/miniconda3
chown -R airflow:airflow /home/airflow/miniconda3
echo "export PATH=\"/home/airflow/miniconda3/bin:\$PATH\"" >> /home/airflow/.bashrc
