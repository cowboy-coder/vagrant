#!/bin/bash
# https://en.opensuse.org/Docker

# Install docker
zypper install -y docker

# Start the docker daemon during boot
systemctl enable docker

# Join the docker group that is allowed to use the docker daemon
#usermod -G docker -a $USER

# Restart the daemon
systemctl restart docker

# Status
systemctl status docker

